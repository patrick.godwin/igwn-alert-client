API
===

This section provides information about the :py:class:`igwn_alert.client` class, which
is the method for accessing the API.

.. automodule:: igwn_alert.client
   :members:
